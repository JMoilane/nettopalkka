<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkkalaskuri</title>
</head>
<body>
<?php
    $brutto = filter_input(INPUT_POST,'bruttopalkka',FILTER_SANITIZE_NUMBER_INT);
    $ennakko = filter_input(INPUT_POST,'ennakonpidatys',FILTER_SANITIZE_NUMBER_INT);
    $tyoelake = filter_input(INPUT_POST,'tyoelake',FILTER_SANITIZE_NUMBER_INT);
    $vakuutus = filter_input(INPUT_POST,'vakuutus',FILTER_SANITIZE_NUMBER_INT);

    $ennakkolasku = ($brutto / 100) * $ennakko;
    $tyoelakelasku = ($brutto / 100) * $tyoelake;
    $vakuutuslasku = ($brutto / 100) * $vakuutus;
    $nettolasku = $brutto - $ennakkolasku - $tyoelakelasku - $vakuutuslasku;
    
    printf("<p>Nettopalkkasi on %.d</p>",$nettolasku);
    printf("<p>Ennakonpidätys: %.1f",$ennakkolasku);
    printf("<p>Vakuutusmaksu: %.1f",$vakuutuslasku);
    printf("<p>Tyoeläkemaksu: %.1f",$tyoelakelasku);
    ?>
</body>
</html>